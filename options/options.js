// Variables of secure state
const safeSelect = document.getElementById("safeSelect");
const safeHttp = document.getElementById("safeHttp");
const safeHttpPort = document.getElementById("safeHttpPort");
const safeHttpProxyAll = document.getElementById("safeHttpProxyAll");
const safeHttps = document.getElementById("safeHttps");
const safeHttpsPort = document.getElementById("safeHttpsPort");
const safeSocks = document.getElementById("safeSocks");
const safeSocksv4 = document.getElementById("safeSocksv4");
const safeSocksv5 = document.getElementById("safeSocksv5");
const safeProxyDNS = document.getElementById("safeProxyDNS");
const safeAutoLogin = document.getElementById("safeAutoLogin");
const safeAutoConfig = document.getElementById("safeAutoConfig");
const safePassthrough = document.getElementById("safePassthrough");

// Variables of unsecure state
const unsafeSelect = document.getElementById("unsafeSelect");
const unsafeHttp = document.getElementById("unsafeHttp");
const unsafeHttpPort = document.getElementById("unsafeHttpPort");
const unsafeHttpProxyAll = document.getElementById("unsafeHttpProxyAll");
const unsafeHttps = document.getElementById("unsafeHttps");
const unsafeHttpsPort = document.getElementById("unsafeHttpsPort");
const unsafeSocks = document.getElementById("unsafeSocks");
const unsafeSocksv4 = document.getElementById("unsafeSocksv4");
const unsafeSocksv5 = document.getElementById("unsafeSocksv5");
const unsafeProxyDNS = document.getElementById("unsafeProxyDNS");
const unsafeAutoLogin = document.getElementById("unsafeAutoLogin");
const unsafeAutoConfig = document.getElementById("safeAutoConfig");
const unsafePassthrough = document.getElementById("unsafePassthrough");

const syncStorage = document.getElementById("syncStorage");
const saveBtn = document.getElementById("saveBtn");

// Restore options
document.addEventListener("DOMContentLoaded", () => {
	browser.storage.sync.get("syncStorage").then((storage) => {
		syncStorage.checked = storage.syncStorage ?? false;

		browser.storage[syncStorage.checked ? 'sync' : 'local'].get({
			safeMode: "manual",
			unsafeMode: "system",
			safeProxySettings: {},
			unsafeProxySettings: {}
		})
			.then((storage) => {
				function configureMode(type, mode, settings) {
					const selectElement = document.getElementById(type + "Select");
					selectElement.value = mode;
					toggleSettings(type, mode);

					switch (mode) {
						case "manual":
							restoreManualSettings(type, settings);
							break;
						case "autoConfig":
							restoreAutoConfigSettings(type, settings);
							break;
					}
				}

				configureMode("safe", storage.safeMode, storage.safeProxySettings);
				configureMode("unsafe", storage.unsafeMode, storage.unsafeProxySettings);
			});
	});
});

// Save options
document.querySelector("form").addEventListener("submit", (e) => {
	e.preventDefault();

	function getProxySettings(type, mode) {
		switch (mode) {
			case "manual":
				return getManualSettings(type);
			case "autoConfig":
				return {
					proxyType: "autoConfig",
					autoConfigUrl: document.getElementById(type + "AutoConfig").value,
					passthrough: document.getElementById(type + "Passthrough").value
				};
			case "none":
				return { proxyType: "none" };
			case "system":
				return {
					proxyType: "system",
					passthrough: document.getElementById(type + "Passthrough").value
				};
			case "autoDetect":
				return {
					proxyType: "autoDetect",
					passthrough: document.getElementById(type + "Passthrough").value
				};
			default:
				return {};
		}
	}

	const safeProxySettings = getProxySettings("safe", safeSelect.value);
	const unsafeProxySettings = getProxySettings("unsafe", unsafeSelect.value);

	if (syncStorage.checked) {
		browser.storage.local.clear();
		browser.storage.sync.set({ syncStorage: true });
	} else {
		browser.storage.sync.clear();
		browser.storage.sync.set({ syncStorage: false });
	}

	browser.storage[syncStorage.checked ? 'sync' : 'local'].set({
		safeProxySettings: safeProxySettings,
		unsafeProxySettings: unsafeProxySettings,
		safeMode: safeSelect.value,
		unsafeMode: unsafeSelect.value
	}).then(() => {
		saveBtn.disabled = true;
		saveBtn.innerText = "Saved!";
		setTimeout(() => {
			saveBtn.disabled = false;
			saveBtn.innerText = "Save";
		}, 3000)
	});
});


// Show/hide settings for "Safe"
safeSelect.addEventListener("change", () => {
	toggleSettings("safe", safeSelect.value);
});

// Show/hide settings for "Unsafe"
unsafeSelect.addEventListener("change", () => {
	toggleSettings("unsafe", unsafeSelect.value);
});

// Get manual settings
function getManualSettings(safeOrUnsafe) {
	let autoLogin = document.getElementById(safeOrUnsafe + "AutoLogin").checked;
	let httpHost = document.getElementById(safeOrUnsafe + "Http").value;
	let httpPort = document.getElementById(safeOrUnsafe + "HttpPort").value;
	let httpProxyAll = document.getElementById(safeOrUnsafe + "HttpProxyAll").checked;
	let passthrough = document.getElementById(safeOrUnsafe + "Passthrough").value.replaceAll(" ", "");
	let proxyDNS = document.getElementById(safeOrUnsafe + "ProxyDNS").checked;
	let socks = document.getElementById(safeOrUnsafe + "Socks").value;
	let socksPort = document.getElementById(safeOrUnsafe + "SocksPort").value;
	let socksVersion = parseInt(document.querySelector('input[name="' + safeOrUnsafe + 'Socks"]:checked').value);
	let ssl = document.getElementById(safeOrUnsafe + "Https").value;
	let sslPort = document.getElementById(safeOrUnsafe + "HttpsPort").value;

	return {
		proxyType: "manual",
		autoLogin: autoLogin,
		http: httpHost != "" ? httpHost + ":" + httpPort : "",
		httpProxyAll: httpProxyAll,
		passthrough: passthrough,
		proxyDNS: proxyDNS,
		socks: socks != "" ? socks + ":" + socksPort : "",
		socksVersion: socksVersion,
		ssl: ssl != "" ? ssl + ":" + sslPort : ""
	}
}

// Restore manual settings
function restoreManualSettings(safeOrUnsafe, proxySettings) {
	if (safeOrUnsafe && proxySettings) {
		document.getElementById(safeOrUnsafe + "AutoLogin").checked = proxySettings.autoLogin ?? "";
		document.getElementById(safeOrUnsafe + "Http").value = proxySettings.http?.split(":")[0] ?? "";
		document.getElementById(safeOrUnsafe + "HttpPort").value = proxySettings.http?.split(":")[1] ?? "";
		document.getElementById(safeOrUnsafe + "HttpProxyAll").checked = proxySettings.httpProxyAll ?? "";
		document.getElementById(safeOrUnsafe + "Passthrough").value = proxySettings.passthrough ?? "";
		document.getElementById(safeOrUnsafe + "ProxyDNS").checked = proxySettings.proxyDNS ?? "";
		document.getElementById(safeOrUnsafe + "Socks").value = proxySettings.socks?.split(":")[0] ?? "";
		document.getElementById(safeOrUnsafe + "SocksPort").value = proxySettings.socks?.split(":")[1] ?? "";
		document.getElementsByName(safeOrUnsafe + "Socks")[proxySettings.socksVersion === 4 ? 0 : 1].checked = true;
		document.getElementById(safeOrUnsafe + "Https").value = proxySettings.ssl?.split(":")[0] ?? "";
		document.getElementById(safeOrUnsafe + "HttpsPort").value = proxySettings.ssl?.split(":")[1] ?? "";
		document.getElementById(safeOrUnsafe + "Passthrough").value = proxySettings.passthrough ?? "";
	}
}

function restoreAutoConfigSettings(safeOrUnsafe, proxySettings) {
	document.getElementById(safeOrUnsafe + "AutoConfig").value = proxySettings.autoConfigUrl
	document.getElementById(safeOrUnsafe + "Passthrough").value = proxySettings.passthrough;
}

// Toggle visibility of divs
function toggleSettings(safeOrUnsafe, settingsMode) {
	const passthroughDiv = document.getElementById(safeOrUnsafe + "PassthroughDiv");
	const settings = document.getElementById(safeOrUnsafe + "Settings");
	const autoConfigSettings = document.getElementById(safeOrUnsafe + "AutoConfigSettings");

	// Grundsätzlich alles ausblenden
	passthroughDiv.style.display = "none";
	settings.style.display = "none";
	autoConfigSettings.style.display = "none";

	// Einstellungen basierend auf settingsMode anzeigen
	switch (settingsMode) {
		case "autoConfig":
			autoConfigSettings.style.display = "block";
			passthroughDiv.style.display = "block";
			break;
		case "manual":
			settings.style.display = "block";
			passthroughDiv.style.display = "block";
			break;
		case "system":
		case "autoDetect":
			passthroughDiv.style.display = "block";
			break;
		case "none":
		default:
			// Alles bleibt ausgeblendet
			break;
	}
}