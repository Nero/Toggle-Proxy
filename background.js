// Request incognito access
let privateAllowed;
browser.extension.isAllowedIncognitoAccess().then(value => privateAllowed = value);

// Restore state
let active;
browser.storage.local.get("active").then((storage) => {
	if (storage.active) activateProxy();
	else deactivateProxy();
});

// Evaluate if settings are synced
let syncStorage;
browser.storage.sync.get("syncStorage").then((storage) => {
	syncStorage = storage.syncStorage ?? false;
});
browser.storage.onChanged.addListener((changes) => {
	if (changes.syncStorage) {
		syncStorage = changes.syncStorage.newValue;
	}
});


function activateProxy() {
	if (privateAllowed) {
		browser.storage[syncStorage ? 'sync' : 'local'].get("safeProxySettings")
			.then(({ safeProxySettings }) => {
				browser.proxy.settings.set({
					value: safeProxySettings
				});

				active = true;

				browser.storage.local.set({
					active: true
				})
					.then(RefreshPageActionIcons);
			});

		browser.action.setIcon({
			path: "icons/Safe.png"
		});
		browser.action.setTitle({
			title: "Turn off proxy"
		});
	}
}

function deactivateProxy() {
	if (privateAllowed) {
		browser.storage[syncStorage ? 'sync' : 'local'].get("unsafeProxySettings")
			.then(({ unsafeProxySettings }) => {
				browser.proxy.settings.set({
					value: unsafeProxySettings
				});

				active = false;

				browser.storage.local.set({
					active: false
				})
					.then(RefreshPageActionIcons);
			});

		browser.action.setIcon({
			path: "icons/Unsafe.png"
		});
		browser.action.setTitle({
			title: "Turn on proxy"
		});
	}
}

// PageAction-Icons aktualisieren
function RefreshPageActionIcons() {
	browser.storage[syncStorage ? 'sync' : 'local'].get({
		safeProxySettings: {
			passthrough: ""
		}
	}).then((storage) => {
		if (storage.safeProxySettings.passthrough) {
			const whitelist = storage.safeProxySettings.passthrough.replaceAll(" ", "").split(",");

			browser.tabs.query({}).then((tabs) => {
				tabs.forEach(tab => {
					const tabDomain = getDomain(tab.url);
					if (whitelist.includes(tabDomain))
						setPageAction(0, tab.id);
					else
						setPageAction(1, tab.id);
				});
			});
		}
	})
}

// Toggle
browser.action.onClicked.addListener(() => {
	if (privateAllowed) {

		browser.storage.local.get("active").then((storage) => {
			if (storage.active) deactivateProxy();
			else activateProxy();
		})

	} else {
		browser.action.setPopup({
			popup: "popup.html"
		});
		browser.action.openPopup();
		browser.action.setPopup({
			popup: ""
		});
	}
});


// Change pageAction when domain is whitelisted
browser.tabs.onUpdated.addListener(() => {
	browser.tabs.query({ active: true }).then((tabs) => {
		browser.storage[syncStorage ? 'sync' : 'local'].get({
			safeProxySettings: {
				passthrough: ""
			}
		}).then((storage) => {
			const tabDomain = getDomain(tabs[0].url);

			if (storage.safeProxySettings.passthrough.includes(tabDomain))
				setPageAction(0, tabs[0].id);
			else
				setPageAction(1, tabs[0].id);
		})
	});
})

// Change pageAction state (0 = whitelisted/red, 1 = not/green)
function setPageAction(state, tabId) {
	let title;
	let icon;

	if (state === 0) {
		title = "Domain is whitelisted"
		icon = active ? "Unsafe" : "UnsafeDisabled"
	} else {
		title = "Domain is NOT whitelisted"
		icon = active ? "Safe" : "SafeDisabled"
	}

	browser.pageAction.setTitle({
		tabId: tabId,
		title: title
	});

	browser.pageAction.setIcon({
		tabId: tabId,
		path: "icons/" + icon + ".png"
	});
}

// PageAction clicked -> Add/Remove to/from whitelist
browser.pageAction.onClicked.addListener((tab) => {
	browser.storage[syncStorage ? 'sync' : 'local'].get({
		safeProxySettings: {},
		unsafeProxySettings: {}
	})
		.then((storage) => {
			let safeOrUnsafe = active ? "safe" : "unsafe";

			if (storage[safeOrUnsafe + "ProxySettings"]) {
				const settings = storage[safeOrUnsafe + "ProxySettings"];
				const tabDomain = getDomain(tab.url);

				if (settings.passthrough.includes(tabDomain)) {
					// Remove
					settings.passthrough = settings.passthrough.replace(tabDomain + ",", "");
					setPageAction(1, tab.id);
				}
				else {
					// Add
					settings.passthrough += tabDomain + ",";
					setPageAction(0, tab.id);
				}

				// Apply settings & store them
				browser.proxy.settings.set({ value: settings });

				let objectToSave = {};
				objectToSave[safeOrUnsafe + 'ProxySettings'] = settings;

				browser.storage[syncStorage ? 'sync' : 'local'].set(objectToSave);
			}
		});
});

function getDomain(url) {
	return new URL(url).hostname.split(".").slice(-2).join(".");
	// return url.match(/^(?:https?:\/\/)?(?:[^.]+\.)*([^.]+\.[^.]+)(?:\/.*)?$/i)[1].split("/")[0];
}